document.addEventListener("DOMContentLoaded", () => {
  const grid = document.querySelector(".grid");
  const moveDisplay = document.getElementById("move");
  const scoreDisplay = document.getElementById("score");
  let rowOfThree = [];
  let btnClose = document.querySelector(".btn-close");
  let btnAgain = document.querySelector(".btn-again");
  let btnWin = document.querySelector(".btn-win");
  let explanationDiv = document.querySelector(".explanation-wrapper");
  let gameOverDiv = document.querySelector(".game-over");
  let winDiv = document.querySelector(".win");
  let starsDiv = document.querySelector(".stars");



  const width = 8;
  const squares = [];
  let move = 15;
  let score = 0;
  let newArr = [];
  moveDisplay.innerHTML = move;

  let candyColors = [
    "url(images/red-candy.png)",
    "url(images/yellow-candy.png)",
    "url(images/orange-candy.png)",
    "url(images/purple-candy.png)",
    "url(images/green-candy.png)",
    "url(images/blue-candy.png)",
  ];

  btnClose.addEventListener("click", function () {
    explanationDiv.classList.add("hidden");
  });
//_______________________________________________________CHECK FOR MORE CONVINIENT SOLUTION FOR BTN 
  btnAgain.addEventListener("click", function () {
    gameOverDiv.classList.add("hidden");
    moveDisplay.innerHTML = 15;
    move = 15;
    score = 0;
    scoreDisplay.innerHTML = score;
  });

  btnWin.addEventListener("click", function () {
    winDiv.classList.add("hidden");
    starsDiv.innerHTML='';
    moveDisplay.innerHTML = 15;
    move = 15;
    score = 0;
    scoreDisplay.innerHTML = score;
  });


  //creating the array with the elements
  for (let idx = 0; idx < 58; idx++) {
    let randomEl = Math.floor(Math.random() * candyColors.length);
    newArr.push(candyColors[randomEl]);
  }

  for (let idx = 0; idx < 4; idx++) {
    newArr.push("url(images/treasure-chest.png)");
  }

  for (let idx = 0; idx < 2; idx++) {
    newArr.push("url(images/Power-Icon.jpg)");
  }

  for (let i = newArr.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    let temp = newArr[i];
    newArr[i] = newArr[j];
    newArr[j] = temp;
  }

  
  //create the board
  function createBoard() {
    for (let i = 0; i < width * width; i++) {
      const square = document.createElement("div");
      square.classList.add("mystyle");
      square.setAttribute("draggable", true);
      square.setAttribute("id", i);

      square.style.backgroundImage = newArr[i];

      if (newArr[i] == "url(images/treasure-chest.png)") {
        square.classList.add("treasure");
      } else if (newArr[i] == "url(images/Power-Icon.jpg)") {
        square.classList.add("power");
      }
      grid.appendChild(square);
      squares.push(square);
    }
  }
  createBoard();

 
  // event listeners for dragging the elements
  let colorBeingDragged;
  let colorBeingReplaced;
  let squareIdBeingDragged;
  let squareIdBeingReplaced;

  squares.forEach((square) => square.addEventListener("dragstart", dragStart));
  squares.forEach((square) => square.addEventListener("dragend", dragEnd));
  squares.forEach((square) => square.addEventListener("dragover", dragOver));
  squares.forEach((square) => square.addEventListener("dragenter", dragEnter));
  squares.forEach((square) => square.addEventListener("drageleave", dragLeave));
  squares.forEach((square) => square.addEventListener("drop", dragDrop));

  //getting the color and the id of the field starting to drag
  function dragStart() {
    colorBeingDragged = this.style.backgroundImage;
    squareIdBeingDragged = parseInt(this.id);
  }

  function dragOver(e) {
    e.preventDefault();
  }

  function dragEnter(e) {
    e.preventDefault();
  }

  //emptying the field that started to leave its' position
  function dragLeave() {
    this.style.backgroundImage = "";
  }

  //replacing the color of the field in which other field is being dragged to
  function dragDrop() {
    colorBeingReplaced = this.style.backgroundImage;
    squareIdBeingReplaced = parseInt(this.id);
    this.style.backgroundImage = colorBeingDragged;
    squares[squareIdBeingDragged].style.backgroundImage = colorBeingReplaced;
  }

  function dragEnd() {
    let validMoves = [
      squareIdBeingDragged - 1, // to be moved on the left
      squareIdBeingDragged - width, //to be moved up
      squareIdBeingDragged + 1, //to be moved right
      squareIdBeingDragged + width, //to be moved down
    ];

    //valid move returns true or false
    let validMove = validMoves.includes(squareIdBeingReplaced);

    if (squareIdBeingReplaced && validMove) {
      // squares[squareIdBeingReplaced].style.backgroundImage = colorBeingReplaced;
      // squares[squareIdBeingDragged].style.backgroundImage = colorBeingDragged;
      //_____________________________________________________ TO DO NEXT :NOT TO BE ABLE TO MOVE ONE EL. NEXT TO THE OTHER WITHOUT MATCHING THREEE
    } else if (squareIdBeingReplaced && !validMove) {
      squares[squareIdBeingReplaced].style.backgroundImage = colorBeingReplaced;
      squares[squareIdBeingDragged].style.backgroundImage = colorBeingDragged;
    } else {
      squares[squareIdBeingDragged].style.backgroundImage = colorBeingDragged;
    }
  }

  function moveIntoSquareBelow() {
    for (i = 0; i < 55; i++) {
      if (squares[i + width].style.backgroundImage === "") {
        squares[i + width].style.backgroundImage =
          squares[i].style.backgroundImage;
        squares[i].style.backgroundImage = "";
        const firstRow = [0, 1, 2, 3, 4, 5, 6, 7];
        const isFirstRow = firstRow.includes(i);

        if (isFirstRow && squares[i].style.backgroundImage === "") {
          let randomColor = Math.floor(Math.random() * newArr.length);
          squares[i].style.backgroundImage = newArr[randomColor];
        }
      }
    }
  }


  //Checking valid moves for matching three elements in a row
  //_________________________________________NEXT TO DO, OPTIMISATION, MAKING THE SPECIAL ELEMENT WORK
  function checkRowForThree(e) {
    for (i = 0; i < 61; i++) {
      rowOfThree = [i, i + 1, i + 2];
      let decidedColor = squares[i].style.backgroundImage;
      const isBlank = squares[i].style.backgroundImage === "";
      const notValid = [6, 7, 14, 15, 22, 23, 30, 31, 38, 39, 46, 47, 54, 55];
      let randoms = Math.floor(Math.random() * candyColors.length);

      if (notValid.includes(i)) continue;
      if (
        rowOfThree.every(
          (index) =>
            squares[index].style.backgroundImage === decidedColor && !isBlank
        )
      ) {
        if (move > 0) {
          move--;
          moveDisplay.innerHTML = move;
          if (moveDisplay.innerHTML == 0) {
            gameOverDiv.classList.remove("hidden");
          }

          if (
            colorBeingDragged == 'url("images/treasure-chest.png")' ||
            colorBeingReplaced == 'url("images/treasure-chest.png")'
          ) {
           
              score++;
              scoreDisplay.innerHTML = score;
              if(score == 5){
                winDiv.classList.remove('hidden');
                if(move> 10){
                  starsDiv.append(`<p>One star</p>`)
                }else if(move > 5 && move <= 10){
                  starsDiv.append(`<p>Two star</p>`)
                }else if(move<=5){
                  starsDiv.append(`<p>Three star</p>`)

                }

              }
              let randoms = Math.floor(Math.random() * candyColors.length);
              let elBeingDragged = document.querySelector(
                `[id='${squareIdBeingDragged}']`
              );
              let elBeingReplaced = document.querySelector(
                `[id='${squareIdBeingReplaced}']`
              );

              if (
                elBeingReplaced.style.backgroundImage ==
                'url("images/treasure-chest.png")'
              ) {
                squares[squareIdBeingReplaced].style.backgroundImage =
                  colorBeingReplaced;
              } else if (
                elBeingDragged.style.backgroundImage ==
                'url("images/treasure-chest.png")'
              ) {
                squares[squareIdBeingDragged].backgroundImage ==
                  candyColors[randoms];
              }
            
          }

          rowOfThree.forEach((index) => {
            squares[index].style.backgroundImage = "";
          });
        }
      }
    }
  }

  // cecking valid moves for matching three elements in a column;
  //______________________________________NEXT TO DO: OPTIMISATION, MAKING THE SPECIAL ELEMENT WORK
  function checkColumnForThree() {
    for (i = 0; i < 47; i++) {
      let columnOfThree = [i, i + width, i + width * 2];
      let decidedColor = squares[i].style.backgroundImage;
      const isBlank = squares[i].style.backgroundImage === "";
      if (
        columnOfThree.every(
          (index) =>
            squares[index].style.backgroundImage === decidedColor && !isBlank
        )
      ) {
        if (move > 0) {
          move--;
          moveDisplay.innerHTML = move;
          if (moveDisplay.innerHTML == 0) {
            gameOverDiv.classList.remove("hidden");
          }
          if (
            colorBeingDragged == 'url("images/treasure-chest.png")' ||
            colorBeingReplaced == 'url("images/treasure-chest.png")'
          ) {
              score++;
              scoreDisplay.innerHTML = score;
              if(score==5){
                winDiv.classList.remove('hidden');
              }
              let randoms = Math.floor(Math.random() * candyColors.length);
              let elBeingDragged = document.querySelector(
                `[id='${squareIdBeingDragged}']`
              );
              let elBeingReplaced = document.querySelector(
                `[id='${squareIdBeingReplaced}']`
              );

              if (
                elBeingReplaced.style.backgroundImage ==
                'url("images/treasure-chest.png")'
              ) {
                squares[squareIdBeingReplaced].style.backgroundImage =
                  colorBeingReplaced;
              } else if (
                elBeingDragged.style.backgroundImage ==
                'url("images/treasure-chest.png")'
              ) {
                squares[squareIdBeingDragged].backgroundImage ==
                  candyColors[randoms];
              }
          }

          columnOfThree.forEach((index) => {
            squares[index].style.backgroundImage = "";
          });
        }
      }
    }
  }

  window.setInterval(function () {
    checkRowForThree();
    checkColumnForThree();
    moveIntoSquareBelow();
  }, 100);
});
